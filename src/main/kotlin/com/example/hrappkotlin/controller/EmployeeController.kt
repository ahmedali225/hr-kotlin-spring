package com.example.hrappkotlin.controller

import com.example.hrappkotlin.model.EmployeeDTO
import com.example.hrappkotlin.model.EmployeeUpdateRequest
import com.example.hrappkotlin.service.DepartmentService
import com.example.hrappkotlin.service.EmployeeService
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.*
import javax.validation.Valid

@RestController
class EmployeeController {
    @Autowired
    lateinit var employeeService: EmployeeService

    @Autowired
    lateinit var departmentService: DepartmentService

    @PostMapping("/employee")
    fun createEmployee(@Valid @RequestBody employeeDTO: EmployeeDTO) =
        employeeService.createEmployee(employeeDTO).map { newEmployee -> ResponseEntity.status(HttpStatus.CREATED).body(newEmployee) }

    @GetMapping("/employee/{id}")
    fun getEmployee(@PathVariable("id") id: String) = employeeService.getEmployee(id)//.switchIfEmpty(Mono.error(Exception("No Employee Found!")))

    @GetMapping("/employee")
    fun getAllEmployees(@RequestParam("minAge", required = false) minAge: Int?
                        , @RequestParam("minSalary" , required = false) minSalary: Double?) = employeeService.getAllEmployees(minAge, minSalary)

    @GetMapping("/departments")
    fun getAllDepartments() = departmentService.getAllDepartments()

    @PutMapping("/employee/{id}")
    fun updateEmployee(@PathVariable("id") id: String, @RequestBody updateRequest: EmployeeUpdateRequest) =
            employeeService.updateEmployee(id, updateRequest).map { updatedEmployee -> ResponseEntity.status(HttpStatus.OK).body(updatedEmployee) }

    @DeleteMapping("/employee", "/employee/{id}")
    fun deleteEmployee(@PathVariable(required = false) id: String?) =
        employeeService.deleteEmployee(id).map { ResponseEntity.noContent() }
}