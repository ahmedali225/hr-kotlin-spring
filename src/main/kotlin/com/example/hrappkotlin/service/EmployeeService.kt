package com.example.hrappkotlin.service

import com.example.hrappkotlin.model.Employee
import com.example.hrappkotlin.model.EmployeeDTO
import com.example.hrappkotlin.model.EmployeeUpdateRequest
import com.example.hrappkotlin.repository.EmployeeRepo
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service
import reactor.core.publisher.Mono

@Service
class EmployeeService {
    @Autowired
    lateinit var employeeRepo: EmployeeRepo

    fun createEmployee(employeeDTO: EmployeeDTO) = employeeRepo.save(EmployeeDTO.newEmployee(employeeDTO))//employeeDb.put(employee.id, employee)

    fun getEmployee(id: String) = employeeRepo.findById(id) //employeeDb[id].toMono()

    fun getAllEmployees(minAge: Int? = null, minSalary: Double? = null) =
            employeeRepo.findAll()
                    .filter { it.age >= minAge ?: Int.MIN_VALUE }
                    .filter { it.salary >= minSalary ?: Double.MIN_VALUE }

    fun updateEmployee(id: String, employeeUpdateRequest: EmployeeUpdateRequest): Mono<Employee> {
        return employeeRepo.findById(id)
                .flatMap {
                    it.department = employeeUpdateRequest.department ?: it.department
                    it.salary = employeeUpdateRequest.salary ?: it.salary
                    employeeRepo.save(it)
                }
    }

    fun deleteEmployee(id: String?): Mono<Void> = if(id != null ) employeeRepo.deleteById(id) else employeeRepo.deleteAll()//employeeDb.remove(id)
}