package com.example.hrappkotlin

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication
import org.springframework.data.cassandra.repository.config.EnableReactiveCassandraRepositories

@SpringBootApplication
@EnableReactiveCassandraRepositories
class HrAppKotlinApplication

fun main(args: Array<String>) {
    runApplication<HrAppKotlinApplication>(*args)
}
