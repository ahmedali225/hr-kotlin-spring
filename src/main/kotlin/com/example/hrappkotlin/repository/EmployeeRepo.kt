package com.example.hrappkotlin.repository

import com.example.hrappkotlin.model.Employee
import org.springframework.data.cassandra.repository.ReactiveCassandraRepository
import org.springframework.stereotype.Repository

@Repository
interface EmployeeRepo: ReactiveCassandraRepository<Employee, String>