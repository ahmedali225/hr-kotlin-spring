Steps to create database if needed:

docker exec -it hrappkotlin_cassandra_1 bash
/opt/bitnami/cassandra$ bin/cqlsh -u cassandra -p cassandra
CREATE SCHEMA hr WITH replication = {'class': 'SimpleStrategy', 'replication_factor': 1};
USE hr;
CREATE TABLE employee (id text PRIMARY KEY , name text , age int, department text , salary double );
INSERT INTO employee (id, name, age, department, salary ) VALUES ( 1, 'Mega', 32, 'ProfessionalServices', 1000.00);


sample json:
{
    "name": "John",
    "age": 21,
    "department": "Developer",
    "salary": 500
}

also, postman test cases can be imported from the postman directory in this project.